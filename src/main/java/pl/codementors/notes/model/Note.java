package pl.codementors.notes.model;

/**
 * Class representing single note. Note consists from title and content.
 * @author psysiu
 */
public class Note {

    /*
    When naming classes, each word should start with big letter.
     */

    /*
    Field is a variable inside the class with access scope (public, protected, private, package-private),
    describing the object inner state. When naming fields (or any variable) all words should start with big letter.
    First word starts always with small letter.
     */

    /**
     * Note title.
     */
    private String title;

    /**
     * Note content.
     */
    private String content;

    /*
    Variables (fields) describing object inner state should be made private and be accessed using methods with
    specified naming.
     */

    /*
    Getters are methods starting with "get" prefix and returned field name.
     */

    /**
     *
     * @return Note title.
     */
    public String getTitle() {
        return title;
    }

    /*
    Setters are methods starting with "set" prefix and name of the field which will be changed.
    They require one parameter which is new value for a field.
     */

    /**
     *
     * @param title New value for note title.
     */
    public void setTitle(String title) {
        /*
        In setters it is common to name the method parameter in the same way as changed field is named. This
        causes that the method parameter hides the field. Keyword "this" allows to access class members.
         */
        this.title = title;
    }

    /**
     *
     * @return Note content.
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content New value for note content.
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * It prints notes title and content to standard output.
     * The way how note is displayed on the console ([] is replaced with proper values):
     * TITLE: [note title]
     * CONTENT: [content]
     */
    public void printToOutput() {
        //HOMEWORK: Fill this with proper use of System.out.
        System.out.println("TITLE: ["+title+"]");
        System.out.println("CONTENT: ["+content+"]");

    }

}

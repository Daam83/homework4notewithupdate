package pl.codementors.notes;

import pl.codementors.notes.model.Note;//Note class is different package so it need to be imported.
import pl.codementors.notes.model.Notepad;

/**
 * Main application class simulating some user interaction.
 * @author psysiu
 */
public class NotesExamples {

    /**
     * Application starting method.
     * @param args Application start params.
     */
    public static void main(String[] args) {

        /*
        Application is going to allow its user to store and read his personal notes.
         */

        /*
        Local variable creation consists of the elements:
        Notepad - type name, in this case it is class name,
        notepad - variable name, can be anything,
        = - assigning operator,
        new - keyword for creating new objects,
        Notepad() - call the class constructor.
         */
        Notepad notepad = new Notepad();

        /*
        In future examples here will be interaction with user using standard input.
        At this moment we are only testing Note and Notepad classes.
         */

        /*
        Lets say that user want to store 2 new notes at some indexes.
         */

        Note note1 = new Note();
        note1.setTitle("Stuff to do:");
        note1.setContent("feed cat, feed dog, go out with dog");
        notepad.setNote(2, note1);//Content and indexes are hardcoded for the sake of example. In future fetched from input.

        Note note2 = new Note();
        note2.setTitle("Stuff to buy:");
        note2.setContent("dog food, cat food, my food (something cheap)");
        notepad.setNote(4, note2);

        /*
        Lets say user want's to fetch some note and display it.
         */

        /*
        Assigning method result to variable consists of:
        Note - type name,
        fetchedNote - variable name, can be anything,
        = - assigning operator,
        notepad - name of the object form which specified method will be called,
        .getNote(2) - method call.
        There is now new keyword because we are not calling any constructor here.
         */
        Note fetchedNote = notepad.getNote(2);
        fetchedNote.printToOutput();

        /*
        Lets say user wan't to print all the notes he has already provided.
         */
        notepad.printNotesToOutput();

    }

}
